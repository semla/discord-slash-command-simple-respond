import express = require("express");
import { middleWare } from "./middleware";
import * as serverless from "serverless-http";

const app = express();
// rawBody needed for nacl, below is from https://flaviocopes.com/express-get-raw-body/
app.use(
  express.json({
    verify: (req, res, buf) => {
      (req as any).rawBody = buf;
    },
  })
);

app.use(middleWare);
// app.use(cors);
// module.exports.handler = serverless(app);
export const handler = serverless(app);
